package ru.uds.lesson1_1;

import org.junit.Assert;
import org.junit.Test;

public class ClasTest {

    @Test
    public void multiplication() {
        Assert.assertEquals(Clas.multiplication(3,4),12);
        Assert.assertEquals(Clas.multiplication(3,-4),-12);
        Assert.assertEquals(Clas.multiplication(-3,-4),12);
        Assert.assertEquals(Clas.multiplication(0,4),0);
        Assert.assertEquals(Clas.multiplication(0,-4),0);
        Assert.assertEquals(Clas.multiplication(-3,10),-30);
        Assert.assertEquals(Clas.multiplication(0,0),0);
    }
}