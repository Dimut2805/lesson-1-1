package ru.uds.lesson1_1;

/**
 * Умножает одно число на другое
 *
 * @author D. Utin 17IT18
 */
public class Clas {
    public static void main(String[] args) {
        System.out.print(multiplication(3, 4));
    }

    /**
     * Умножение чисел
     *
     * @param a - Первый множитель
     * @param b - Второй множитель
     * @return - Результат перемножения
     */
    public static int multiplication(int a, int b) {
        if (a == 0 || b == 0) {
            return 0;
        }
        int repetition = a < b ? Math.abs(a) : Math.abs(b); //Повторения
        int value = a < b ? Math.abs(b) : Math.abs(a); //Число
        boolean boolNegativeNumbers = (a > 0 && b < 0) || (a < 0 && b > 0);
        boolean boolFlag = repetition % 2 != 0;
        if (repetition % 2 != 0) {
            repetition -= 1;
        }
        int work = 0; //Возвращаемы результат
        int sumValue = value + value;
        repetition = repetition / 2;
        for (int i = 0; i < repetition; i++) {
            work += sumValue;
        }
        if (boolFlag) {
            work += value;
        }
        if (boolNegativeNumbers) {
            work = -work;
        }
        return work;
    }
}